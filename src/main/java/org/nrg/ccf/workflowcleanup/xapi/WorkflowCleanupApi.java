package org.nrg.ccf.workflowcleanup.xapi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.ProjectId;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.om.WrkWorkflowdata;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xft.search.CriteriaCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@XapiRestController
@Api(description = "Workflow Cleanup API")
public class WorkflowCleanupApi extends AbstractXapiRestController {

	private static final Logger _logger = LoggerFactory.getLogger(WorkflowCleanupApi.class);
	private final Comparator<Map<String,String>> mapCompare = new Comparator<Map<String,String>>() {
		@Override
		public int compare(Map<String, String> o1, Map<String, String> o2) {
			int comp = o1.get("label").compareTo(o2.get("label"));
			if (comp!=0) {
				return comp;
			}
			comp = o1.get("workflowId").compareTo(o2.get("workflowId"));
			return comp;
		}};

	@Autowired
	protected WorkflowCleanupApi(UserManagementServiceI userManagementService, RoleHolder roleHolder) {
		super(userManagementService, roleHolder);
	}
	
	@ApiOperation(value = "Gets problem workflows", notes = "Gets problem workflows.",
			responseContainer = "List", response = Map.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/workflowCleanup/project/{projectId}/problemWorkflows"}, restrictTo=AccessLevel.Read,
    						produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Map<String,String>>> problemWorkflows(@PathVariable("projectId") @ProjectId final String projectId,
    			@RequestParam(value="olderThanDays", defaultValue="3") final Integer olderThanDays) throws NrgServiceException {
		final CriteriaCollection cc= new CriteriaCollection("AND");
		try {
			cc.addClause("wrk:workflowData.ExternalID",projectId);
			cc.addClause("wrk:workflowData.data_type","xnat:mrSessionData");
			final CriteriaCollection subCC = new CriteriaCollection("OR");
			subCC.addClause("wrk:workFlowData.status","In Progress");
			subCC.addClause("wrk:workFlowData.status","HOLD");
			subCC.addClause("wrk:workFlowData.status","Running");
			subCC.addClause("wrk:workFlowData.status","Queued");
			cc.addClause(subCC);
			cc.addClause("wrk:workflowData.launch_time","<",DateUtils.addDays(new Date(), olderThanDays*-1));
			List<WrkWorkflowdata> workflows =  WrkWorkflowdata.getWrkWorkflowdatasByField(cc, getSessionUser(), false);
			final CriteriaCollection expCC = new CriteriaCollection("OR");
			for (final WrkWorkflowdata workflow : workflows) {
				expCC.addClause("xnat:experimentData.id",workflow.getId());
			}
			List<XnatExperimentdata> exps = XnatExperimentdata.getXnatExperimentdatasByField(expCC, getSessionUser(), false);
			Map<String,String> labelMap = new HashMap<>();
			for (XnatExperimentdata exp : exps) {
				labelMap.put(exp.getId(), exp.getLabel());
			}
			// The JSON parser couldn't handle a list of WrkWorkflowdata, so let's just pull out what we want into a Map.
			List<Map<String,String>> returnList = new ArrayList<>();
			for (final WrkWorkflowdata workflow : workflows) {
				boolean hasMatchingExperiment = false;
				final Map<String,String> map = new HashMap<>();
				map.put("workflowId", workflow.getWorkflowId().toString());
				map.put("dataType", workflow.getDataType());
				map.put("id", workflow.getId());
				if (labelMap.containsKey(workflow.getId())) {
					map.put("label", labelMap.get(workflow.getId()));
					// NOTE:  Some workflows may exist for deleted experiments.  We won't include those in the list.
					hasMatchingExperiment = true;
				} 
				map.put("externalId", workflow.getExternalid());
				map.put("externalId", workflow.getExternalid());
				map.put("status", workflow.getStatus());
				map.put("pipelineName", workflow.getPipelineName());
				map.put("launchTime", workflow.getLaunchTime().toString());
				map.put("comments", workflow.getComments());
				map.put("details", workflow.getDetails());
				map.put("justification", workflow.getJustification());
				map.put("description", workflow.getDescription());
				map.put("src", workflow.getSrc());
				map.put("type", workflow.getType());
				map.put("category", workflow.getCategory());
				if (hasMatchingExperiment) {
					returnList.add(map);
				}
			}
			Collections.sort(returnList,mapCompare);
			
			return new ResponseEntity<List<Map<String,String>>>(returnList,HttpStatus.OK);
		} catch (Exception e) {
			_logger.error("Error getting workflows - ", e);
			return new ResponseEntity<List<Map<String,String>>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@ApiOperation(value = "Sets workflow status to Failed (Dismissed)", notes = "Sets workflow status to Failed (Dismissed).",
			response = Void.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/workflowCleanup/project/{projectId}/cleanupWorkflows"}, restrictTo=AccessLevel.Member,
    						consumes = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> cleanupWorkflows(@PathVariable("projectId") @ProjectId final String projectId,
    			@RequestBody final List<String> idList) throws NrgServiceException {
		final CriteriaCollection cc= new CriteriaCollection("AND");
		try {
			cc.addClause("wrk:workflowData.ExternalID",projectId);
			cc.addClause("wrk:workflowData.data_type","xnat:mrSessionData");
			final CriteriaCollection subCC = new CriteriaCollection("OR");
			subCC.addClause("wrk:workFlowData.status","In Progress");
			subCC.addClause("wrk:workFlowData.status","HOLD");
			subCC.addClause("wrk:workFlowData.status","Running");
			subCC.addClause("wrk:workFlowData.status","Queued");
			cc.addClause(subCC);
			List<WrkWorkflowdata> workflows =  WrkWorkflowdata.getWrkWorkflowdatasByField(cc, getSessionUser(), false);
			for (final WrkWorkflowdata workflow : workflows) {
				if (idList.contains(workflow.getWorkflowId().toString())) {
					workflow.setStatus("Failed (Dismissed)");
					workflow.save(getSessionUser(), false, false, null);
				}
			}
			return new ResponseEntity<Void>(HttpStatus.OK);
		} catch (Exception e) {
			_logger.error("Error updating workflows - ", e);
			return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}

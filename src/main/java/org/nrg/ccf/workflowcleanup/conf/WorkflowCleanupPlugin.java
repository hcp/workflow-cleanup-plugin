package org.nrg.ccf.workflowcleanup.conf;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "workflowCleanupPlugin",
			name = "Workflow Cleanup Plugin"
		)
@ComponentScan({ 
		"org.nrg.ccf.workflowcleanup.conf",
		"org.nrg.ccf.workflowcleanup.xapi"
	})
public class WorkflowCleanupPlugin {
	
	public static Logger logger = Logger.getLogger(WorkflowCleanupPlugin.class);

	public WorkflowCleanupPlugin() {
		logger.info("Configuring the Workflow Cleanup Plugin.");
	}
	
}


if (typeof CCF === 'undefined') {
  CCF = {};
}

var getSearchParams = function(k){
 var p={};
 location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi,function(s,k,v){p[k]=v})
 return k?p[k]:p;
}

if (typeof CCF.workflowcleanup === 'undefined') {
  CCF.workflowcleanup = { };
  CCF.workflowcleanup.project = getSearchParams("project");
  CCF.workflowcleanup.TABLE_URL = serverRoot+"/xapi/workflowCleanup/project/"+CCF.workflowcleanup.project+"/problemWorkflows";
  CCF.workflowcleanup.CLEANUP_URL = serverRoot+"/xapi/workflowCleanup/project/"+CCF.workflowcleanup.project+"/cleanupWorkflows";
}

CCF.workflowcleanup.initialize = function(projectId) {
  console.log("WorkflowCleanup initialize called");
}


CCF.workflowcleanup.renderWorkflowCleanupPanel = function() {
  CCF.workflowcleanup.includeDays()
}

CCF.workflowcleanup.doSelectAll = function(ele) {
    if(ele.checked) {
      $(':checkbox').each(function() {
        // Make sure it's not a hidden/filtered row
        var $row = $(this).closest('tr')
        // console.log($row.css("display"))
        if ($row.css("display") === "table-row") {
          this.checked = true;
        }
      });
    } else {
      $(':checkbox').each(function() {
        this.checked = false;
      });
    }
    CCF.workflowcleanup.shouldShowButton();
}

CCF.workflowcleanup.includeDays = function() {
	var olderThanDays = parseInt($("#include-days").val());
	if (isNaN(olderThanDays)) {
		olderThanDays = 3;
	}
	$("#include-days").val(olderThanDays);
	CCF.workflowcleanup.olderThanDays = olderThanDays;
	CCF.workflowcleanup.renderWorkflowCleanupTable();
	CCF.workflowcleanup.shouldShowButton();
}

CCF.workflowcleanup.fixSelectedWorkflows = function() {

    var fixArr = [];
    $(':checkbox').each(function() {
        if (this.checked) {
           fixArr.push(this.id.substring(2));
	}
    });

    XNAT.ui.banner.top(3000, 'Cleanup process submitted.  The table will reload when processing completes.', 'success');

    //console.log(fixArr);
    XNAT.xhr.post({
	//type: "POST",
	url:CCF.workflowcleanup.CLEANUP_URL,
	cache: false,
	async: true,
	contentType:"application/json; charset=utf-8",
	context: this,
	data: JSON.stringify(fixArr),
	dataType: 'json'
    }).done( function(data, textStatus, jqXHR) {
  	$(".top-banner").hide();
	XNAT.ui.banner.top(3000, 'Workflows successfully updated.', 'success');
    }).fail( function(data, textStatus, jqXHR) {
		xmodal.message("Error","ERROR:  Could not update the workflows. (STATUS=" + textStatus + ").");
    }).always( function(data) {
    	CCF.workflowcleanup.renderWorkflowCleanupTable();
        CCF.workflowcleanup.shouldShowButton();
    });

}

CCF.workflowcleanup.shouldShowButton = function() {
    var hasChecked = false;
    $(':checkbox').each(function() {
        if (this.checked) {
                hasChecked = true;
		return false;
	}
    });
    if (hasChecked) {
        $("#fixButton").removeAttr("disabled", "disabled");
    } else {
        $("#fixButton").attr("disabled", "disabled");
    }
}

CCF.workflowcleanup.renderWorkflowCleanupTable = function() {

  $("#workflow-count-container").hide()
  $("#fixButton").hide();
  CCF.workflowcleanup.allWorkflows = []

  $('#workflow-table-container').empty()
  var loadingMsg = "Loading problem workflows"
  // xmodal doesn't seem to be loaded at this point for whatever reason
  // dialog.loading works, but can't close since there's no dataTable.done() method
  // xmodal.loading.open(loadingMsg)
  // XNAT.ui.dialog.loading.open()

  var tabindex = 0

  XNAT.xhr.get({
  	url: CCF.workflowcleanup.TABLE_URL + "?olderThanDays=" + CCF.workflowcleanup.olderThanDays,
	cache: false,
	async: true,
	context: this,
  }).done( function(data, textStatus, jqXHR) {

	  var returnedRows = data.length;
	  if (returnedRows<1) {
	  	$('#workflow-table-container').html("<div style='color:green;margin-left:10px;margin-top:10px;'><h3>No problem workflows detected!  No cleanup needed.</h3></div>");
		return;
          }

	  $("#fixButton").show();
	  XNAT.table.dataTable(data, {
	    //url: CCF.workflowcleanup.TABLE_URL + "?olderThanDays=" + CCF.workflowcleanup.olderThanDays,
	    table: {
	      id: 'workflowcleanup-' + CCF.workflowcleanup.project,
	      className: 'workflow-table highlight'
	    },
	    messages: {
	       noData: "<div style='color:green;margin-left:10px;margin-top:10px;'><h3>No problem workflows detected!  No cleanup needed.</h3></div>",
	       error: "<div style='color:red;margin-left:10px;margin-top:10px;'><h3>An error occurred retrieving data</h3></div>" 
	    },
	    columns: {
	      run: {
	        label: '<input id="select-all" name="select-all" onchange="CCF.workflowcleanup.doSelectAll(this)" type="checkbox">',
	        td: {'className': 'center'},
	        apply: function() {
	
	          CCF.workflowcleanup.allWorkflows.push(this)
	          tabindex++
	
	          return (
	            '<input id="CB' + this.workflowId + '" ' +
	                    'name="workflow-check" ' +
	                    'class="chkbox" ' +
	                    'type="checkbox" ' +
	                    'tabindex="'+tabindex+'">'
	          )
	        }
	      },
	      workflowId: {
	        label: 'WorkflowID',
	        filter: true,
	        sort: true,
	        td: {'className': 'workflowId center'},
	      },
	      id: {
	        label: 'ExpID',
	        filter: true,
	        sort: true,
	        td: {'className': 'id center'},
	        apply: function(workflow) {
	          var html = workflow;
	          href = '/data/experiments/'+workflow+'?format=html'
	          html = '<a href='+href+' target="_blank" >' + workflow + '</a>'
	          return html
	        }
	      },
	      label: {
	        label: 'Label',
	        filter: true,
	        sort: true,
	        td: {'className': 'label center'},
	        apply: function(workflow) {
	          var html = workflow;
	          href = '/data/projects/'+CCF.workflowcleanup.project+'/experiments/'+workflow+'?format=html'
	          html = '<a href='+href+' target="_blank" >' + workflow + '</a>'
	          return html
	        }
	      },
	      dataType: {
	        label: 'DataType',
	        filter: true,
	        sort: true,
	        td: {'className': 'dataType center'},
	      },
	      pipelineName: {
	        label: 'PipelineName',
	        filter: true,
	        sort: true,
	        td: {'className': 'pipelineName center'},
	      },
	      launchTime: {
	        label: 'LaunchTime',
	        filter: true,
	        sort: true,
	        td: {'className': 'launchTime center'},
	      },
	      status: {
	        label: 'Status',
	        filter: true,
	        sort: true,
	        td: {'className': 'status center'},
	      },
	      category: {
	        label: 'Category',
	        filter: true,
	        sort: true,
	        td: {'className': 'category center'},
	      },
	      type: {
	        label: 'Type',
	        filter: true,
	        sort: true,
	        td: {'className': 'type center'},
	      },
	      comments: {
	        label: 'Comments',
	        filter: true,
	        sort: true,
	        td: {'className': 'comments center'},
	      },
	      justification: {
	        label: 'Justification',
	        filter: true,
	        sort: true,
	        td: {'className': 'justification center'},
	      }
	    }
	  }).render($('#workflow-table-container'))

	  $("#workflow-count-container").html("<em>" + returnedRows + " rows returned</em>")
	  $("#workflow-count-container").show()
  	  initShiftSelect() 

  }).fail( function(data, textStatus, jqXHR) {
		xmodal.message("Error","ERROR:  Could not build the workflow table.");
  });

$(".data-table-wrapper").css({
	'overflow-x': 'auto',
	'height': '100%'
});


}

function initShiftSelect() {
  var lastChecked = null;
  var $chkboxes = $('.chkbox')

  $chkboxes.click(function(e) {
      if(!lastChecked) {
          lastChecked = this
          CCF.workflowcleanup.shouldShowButton();
          return
      }
      if(e.shiftKey) {
        var start = $chkboxes.index(this)
        var end = $chkboxes.index(lastChecked)

        $chkboxes.slice(Math.min(start,end), Math.max(start,end)+ 1).prop('checked', lastChecked.checked)
      }
      lastChecked = this;
      CCF.workflowcleanup.shouldShowButton();
  });
}

// set up custom filter menus
function filterMenuElement(prop){
  if (!prop) return false;
  // call this function in context of the table
  var $workflowTable = $(this);
  var FILTERCLASS = 'filter-' + prop;
  return {
    id: 'workflowcleanup-filter-select-' + prop,
    on: {
      change: function() {
        var selectedValue = $(this).val();
        // console.log(selectedValue);

        $workflowTable.find('button[class*="filter-'+prop+'"]').each(function() {
          var $row = $(this).closest('tr');
          // console.log($row)
          if (selectedValue === 'all') {
            $row.removeClass(FILTERCLASS);
            return;
          }

          $row.addClass(FILTERCLASS);
          // if (selectedValue == this.textContent) {
          if (this.textContent.includes(selectedValue)) {
            $row.removeClass(FILTERCLASS);
          }
        })
      }
    }
  };
}

